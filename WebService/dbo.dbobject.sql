﻿use websvctst
go

CREATE TABLE dbobject
(
	[serialnum] INT NOT NULL,
    [code] INT NULL, 
    [value] NVARCHAR(MAX) NULL
)
