﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace WebService
{
    public class DbAccess
    {
        private string _connectionString;
        public DbAccess(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void StoreDbObjects(InputDbObject[] dbObjects)
        {
            if (dbObjects == null)
                return;

            Array.Sort<InputDbObject>(dbObjects, delegate (InputDbObject objA, InputDbObject objB)
            {
                return objA.code.CompareTo(objB.code);
            });

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (SqlTransaction tx = connection.BeginTransaction())
                {
                    using (SqlCommand cmd = new SqlCommand("TRUNCATE TABLE [websvctst].[dbo].[dbobject]", connection))
                    {
                        cmd.Transaction = tx;
                        cmd.ExecuteNonQuery();
                    }

                    using (SqlCommand cmd = connection.CreateCommand())
                    {
                        cmd.Transaction = tx;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = "INSERT INTO [websvctst].[dbo].[dbobject] ([serialnum], [code], [value]) VALUES (@serialnum, @code, @value);";

                        cmd.Parameters.Add(new SqlParameter("@serialnum", SqlDbType.Int));
                        cmd.Parameters.Add(new SqlParameter("@code", SqlDbType.Int));
                        cmd.Parameters.Add(new SqlParameter("@value", SqlDbType.NVarChar));
                        try
                        {
                            int serialnum = 0;
                            foreach (var dbObject in dbObjects)
                            {
                                cmd.Parameters[0].Value = serialnum++;
                                cmd.Parameters[1].Value = dbObject.code;
                                cmd.Parameters[2].Value = dbObject.value;
                                if (cmd.ExecuteNonQuery() != 1)
                                {
                                    throw new InvalidProgramException();
                                }
                            }
                            tx.Commit();
                        }
                        catch (Exception crap)
                        {
                            tx.Rollback();
                            throw;
                        }
                    }
                }
            }
        }

        public List<DbObject> ReadDbObjects()
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand("SELECT [serialnum], [code], [value] FROM [websvctst].[dbo].[dbobject]", connection))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader != null)
                        {
                            List<DbObject> output = new List<DbObject>();
                            while (reader.Read())
                            {
                                output.Add(new DbObject() { serialnum = (int)reader["serialnum"], code = (int)reader["code"], value = (string)reader["value"] });
                            }
                            return output;
                        }
                    }
                }
            }
            return null;
        }
    }
}
