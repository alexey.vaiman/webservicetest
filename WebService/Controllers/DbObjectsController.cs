﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DbObjectsController : ControllerBase
    {
        static DbAccess _dbAccess;

        [HttpPost]
        public void StoreDbObjects([FromBody] InputDbObject[] inputDbObjects)
        {
            if (_dbAccess == null)
                _dbAccess = new DbAccess(Program.ConnectionString);

            _dbAccess.StoreDbObjects(inputDbObjects);
        }

        [HttpGet]
        public ActionResult<IEnumerable<DbObject>> ReadDbObjects()
        {
            if (_dbAccess == null)
                _dbAccess = new DbAccess(Program.ConnectionString);
            return _dbAccess.ReadDbObjects();
        }
    }
}
