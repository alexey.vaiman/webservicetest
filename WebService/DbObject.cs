﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService
{
    public class InputDbObject
    {
        public int code { get; set; }
        public string value { get; set; }
    }

    public class DbObject : InputDbObject
    {
        public int serialnum { get; set; }
    }
}
